drop table uzytkownik if exists;
create table uzytkownik (
  id INT IDENTITY PRIMARY KEY,
  nick VARCHAR(100) NOT NULL,
  mail VARCHAR(100) NOT NULL,
  pass VARCHAR(100) NOT NULL,
);

drop table follower if exists;
create table follower (
  id INT IDENTITY PRIMARY KEY,
  follower INT NOT NULL,
  following INT NOT NULL,
);

drop table post if exists;
create table post (
  id INT IDENTITY PRIMARY KEY,
  autor INT NOT NULL,
  data_utworzenia DATETIME NOT NULL,
  data_edycji DATETIME,
  tytul VARCHAR(200) NOT NULL,
  tresc VARCHAR(2000) NOT NULL,
  public BOOLEAN NOT NULL
);