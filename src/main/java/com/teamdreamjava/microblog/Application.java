package com.teamdreamjava.microblog;

import spark.ModelAndView;
import spark.template.freemarker.FreeMarkerEngine;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;

/**
 * Hello world!
 */
public class Application {
    public static void main(String[] args) {
        port(8080);
        staticFileLocation("/public");

        get("/", (req, res) -> {
            Map<String, Object> data = new HashMap<>();
            data.put("message", "Test wiadomości");
            data.put("message2", "Tu powinno być H2.");

            return new ModelAndView(data, "hello.ftl");
        }, new FreeMarkerEngine());

        get("/hello/:name", (req, res) -> {
            return "Hello: " + req.params(":name");
        });
    }
}
