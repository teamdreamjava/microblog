import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;


@Entity
public class post {

    @Id
    private int id;

    @Column(nullable = false)
    private int autor;

    @Column(nullable = false)
    private LocalDateTime data_utworzenia;

    @Column(nullable = false)

    private LocalDateTime data_edycji;

    @Column(nullable = false, length = 200)
    private String tytul;

    @Column(nullable = false, length = 200)
    private String tresc;

    @Column(nullable = false, name = "public")
    private boolean publiczny;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAutor() {
        return autor;
    }

    public void setAutor(int autor) {
        this.autor = autor;
    }

    public LocalDateTime getData_utworzenia() {
        return data_utworzenia;
    }

    public void setData_utworzenia(LocalDateTime data_utworzenia) {
        this.data_utworzenia = data_utworzenia;
    }

    public LocalDateTime getData_edycji() {
        return data_edycji;
    }

    public void setData_edycji(LocalDateTime data_edycji) {
        this.data_edycji = data_edycji;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public boolean isPubliczny() {
        return publiczny;
    }

    public void setPubliczny(boolean publiczny) {
        this.publiczny = publiczny;
    }
}
