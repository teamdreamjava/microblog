import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class uzytkownik {

    @Id
    private int id;

    @Column(nullable = false, length = 100, unique = true)
    private String nick;

    @Column(nullable = false, length = 100, unique = true)
    private String mail;

    @Column(nullable = false, length = 100)
    private String pass;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
