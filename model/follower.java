import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class follower {

    @Id
    private int id;

    @Column(nullable = false)
    private int follower;

    @Column(nullable = false)
    private int following;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFollower() {
        return follower;
    }

    public void setFollower(int follower) {
        this.follower = follower;
    }

    public int getFollowing() {
        return following;
    }

    public void setFollowing(int following) {
        this.following = following;
    }
}
